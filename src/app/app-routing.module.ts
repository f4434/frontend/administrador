import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
    children: [
     {
       path: 'login',
       loadChildren: () => import('./login/login.module').then(x => x.LoginModule)
     },
     {
       path: 'home',
       loadChildren: () => import('./home/home.module').then(x => x.HomeModule)
     }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
